import os
import time
from celery import Celery

celery_app = Celery('tasks')
default_config = 'celeryconfig'
celery_app.config_from_object(default_config)