import os
import time

from celery import states
from fastapi import FastAPI, UploadFile, File
from pydantic import BaseModel
from fastapi.responses import FileResponse
from worker import celery_app


class ProcessData(BaseModel):
    data1: str
    data2: str
    data3: str


app = FastAPI()


@app.post("/data_process")
async def test_process(data: ProcessData):
    task = celery_app.send_task('tasks.data_process', args=[data.data1, data.data2, data.data3])
    response = {'task': task.id}
    return response


@app.get("/get_result/{id}")
def get_result(id: str):
    res = celery_app.AsyncResult(id, app=celery_app)
    if res.state == states.PENDING:
        return res.state
    else:
        return dict(state=res.state, count=len(res.result), result=res.result)


@app.post("/file_process")
def file_process(file: UploadFile = File(...)):
    try:
        filename = f'/upload/{file.filename}'
        contents = file.file.read()
        with open(filename, 'wb') as f:
            f.write(contents)
        task = celery_app.send_task('tasks.file_process',args=[filename])
    except Exception:
        return {"message": "There was an error uploading the file"}
    finally:
        file.file.close()

    return {"message": f"Successfully uploaded {file.filename}",'task':task.id}


@app.get("/get_file/{id}")
def get_file(id: str):
    filename = f'/download/{time.time()}-result.csv'
    res = celery_app.AsyncResult(id, app=celery_app)
    if res.state == states.PENDING:
        with open(filename, 'w') as f:
            f.write("Task pending")
    else:
        with open(filename, 'w') as f:
            f.write(res.result)
    return FileResponse(filename,media_type='application/octet-stream',filename=filename)
