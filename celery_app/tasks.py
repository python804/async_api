import os
import time
from celery import Celery, states
from celery.result import AsyncResult

celery_app = Celery('tasks')
default_config = 'celeryconfig'
celery_app.config_from_object(default_config)



@celery_app.task(name='tasks.data_process', track_task_started=True, ignore_result=False)
def data_process_task(user: str, pwd: str, project: str) -> dict:
    time.sleep(5)
    count = 0
    results = []
    for count in range(0, 100):
        results.append({
            'id': count,
            'user': f'{user}_{count}',
            'pwd': f'{pwd}_{count}',
            'project': f'{project}_{count}'
        })

    return results


@celery_app.task(name='tasks.file_process', track_task_started=True, ignore_result=False)
def file_process_task(filename: str):
    data = ''
    with open(filename, 'r') as file:
        data = file.read().replace('\n', '').replace(',','').replace(';','')
    return data
